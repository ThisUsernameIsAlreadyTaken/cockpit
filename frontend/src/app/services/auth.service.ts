import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthGuard} from '../guards/auth.guard';
import {Observable} from 'rxjs';
import {ProtocolItem, Resource} from '../components/management/management.component';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public screens: Resource[] = [];
  public user = 'Bobo';
  public ip = 'http://localhost';

  constructor(private http: HttpClient, private guard: AuthGuard, private router: Router) {
    this.getResources().subscribe((res: Resource[]) => {
      this.screens = res.sort((el1, el2) => {
        return el1.scr - el2.scr;
      });
    });
  }

  public login(name: string, password: string): boolean {
    console.log('login');
    this.user = name;
    this.guard.loggedIn();
    return true;
  }

  public logout() {
    //this.guard.logout();
    //this.router.navigateByUrl('');
  }

  public getCurrentScreen(): Observable<any> {
    return this.http.get(this.ip + ':3000/currentScreen');
  }

  public setCurrentScreen(id: string): Observable<any> {
    return this.http.post(this.ip + ':3000/currentScreen', {value: id});
  }

  public getResources(): Observable<any> {
    return this.http.get(this.ip + ':3000/media');
  }

  public sendResource(screen: number, image: string | ArrayBuffer) {
    console.log('sending resource');
    return this.http.post(this.ip + ':3000/media/', {scr: screen, content: image});
  }

  public deleteResource(rid: string) {
    return this.http.delete(this.ip + ':3000/media/' + rid);
  }

  public updateResource(rid: string, screen: number) {
    return this.http.patch(this.ip + ':3000/media/' + rid, {scr: screen});
  }

  public getProtocol(): Observable<any> {
    return this.http.get(this.ip + ':3000/protocol');
  }

  public updateProtocol(msg: string) {
    console.log('try to update protocol');
    const protItem: ProtocolItem = {
      ts: new Date(),
      name: this.user,
      content: msg
    };
    this.http.post(this.ip + ':3000/protocol/', protItem).subscribe(res => {
      console.log(res);
    });
  }


}
