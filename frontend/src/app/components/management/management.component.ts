import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Socket} from 'ngx-socket-io';

export interface Resource {
  id: string;
  scr: number;
  content: string | ArrayBuffer;
}

export interface ProtocolItem {
  ts: Date;
  name: string;
  content: string;
}

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css'],
})

export class ManagementComponent implements OnInit {

  private image: string | ArrayBuffer = null;
  public currentScreen: string;
  public selectedScreen = '-1';
  public socket: Socket;
  public protocol: ProtocolItem[];
  public ready = false;

  constructor(public auth: AuthService) {
    this.socket = new Socket({url: this.auth.ip + ':3001', options: {name: this.auth.user}});
    this.socket.on('user-connected', msg => {
      console.log('user connected');
    });
    this.socket.on('resource-added', (msg: any) => {
      console.log('resource-added');
      this.loadResources();
      this.getCurrentScreen();
    });
    this.socket.on('current-screen', msg => {
      this.getCurrentScreen();
    });
    this.socket.on('disconnect', msg => {
      this.auth.updateProtocol('left the conference');
    });
    this.loadResources();
    this.getCurrentScreen();
    this.auth.updateProtocol('joined the conference');
  }

  ngOnInit() {

  }

  onFileSelect(event) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.image = reader.result;
      if (this.selectedScreen !== '-1') {
        const index = this.screens.indexOf(this.getScreen(this.selectedScreen));
        this.auth.deleteResource(this.selectedScreen).subscribe(r => {
          console.log('Delete: ' + r);
          this.auth.sendResource(index, this.image).subscribe((res: Resource) => {
            this.socket.emit('resource-added');
            this.selectedScreen = res.id;
            console.log('Added after delete: ' + res);
            this.auth.updateProtocol('Replaced Screen: ' + this.selectedScreen);
          });
        });
      } else {
        this.auth.sendResource(this.screens.length, this.image).subscribe((res: Resource) => {
          this.socket.emit('resource-added');
          console.log('Added: ' + res);
          this.selectedScreen = res.id;
          this.auth.updateProtocol('Added Screen: ' + this.selectedScreen);
        });
      }
    }, false);
    if (event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  loadResources() {
    this.auth.getResources().subscribe((res: Resource[]) => {
      this.screens = res.sort((el1, el2) => {
        return el1.scr - el2.scr;
      });
      this.ready = true;
    });
  }

  getCurrentScreen() {
    this.auth.getCurrentScreen().subscribe((res: any) => {
      this.currentScreen = res.value;
      console.log(res.value);
    });
  }

  getScreen(id: string): Resource {
    return this.screens.find(screen => {
      return screen.id === id;
    });
  }

  setCurrentScreen(id: string) {
    this.auth.setCurrentScreen(id).subscribe(res => {
      this.socket.emit('current-screen');
      this.auth.updateProtocol('Changed focused Screens');
    });
  }

  onRighClick(event, id: string) {
    event.preventDefault();
    if (this.selectedScreen !== id) {
      const oldScr = this.getScreen(id).scr;
      this.auth.updateResource(id, this.getScreen(this.selectedScreen).scr).subscribe(res => {
        this.auth.updateResource(this.selectedScreen, oldScr).subscribe(res2 => {
          console.log('updated');
          this.socket.emit('resource-added');
          this.auth.updateProtocol('Moved Screens');
        });
      });
    }
  }

  openProtocol() {
    this.auth.getProtocol().subscribe((res: ProtocolItem[]) => {
      this.protocol = res;
      this.selectedScreen = '-2';
    });
  }

  getTimeStamp(ts: Date): string {
    return ts.toLocaleString();
  }

  get user() {
    if (this.auth.user === '') {
      this.auth.logout();
    }
    return this.auth.user;
  }

  get Ready(): boolean {
    return this.ready;
  }

  get screens(): Resource[] {
    return this.auth.screens;
  }

  set screens(scr: Resource[]) {
    this.auth.screens = scr;
  }
}
