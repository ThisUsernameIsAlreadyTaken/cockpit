var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

http.listen(3001, function () {
    console.log('listening on *:3001');
});


io.on('connection', function (socket, user) {
    socket.user = user;
    console.log('a user connected');
    socket.broadcast.emit('user-connected', socket.user);
    socket.on('disconnect', function () {
        console.log('user-disconnected');
        socket.broadcast.emit('user-disconnected', socket.user);
    });
    socket.on('resource-added', function (msg) {
        console.log('message: ' + msg);
        io.emit('resource-added', msg);
    });
    socket.on('current-screen', function(msg) {
        console.log('current screen: ' + msg);
        io.emit('current-screen', msg);
    })
});